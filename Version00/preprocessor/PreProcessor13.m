%Main01
% by Ramin Kaviani
clc;close all;clear;

inputExcel='.\Input\Input07.xlsx';
numRows=8; % 23; %1 29 39 23 9 14 14 8
tirIX=64; % fixed
tirIY=512; % fixed
fifIX=128;% fixed
fifIY=128;% fixed
fpsTIR=180371.352785;
fpsFIF=85106.382979;
matrixFilename='.\Output\Matrix.avi';
videoFileName='.\Output\Whole.avi';
videoFileNamePartial='.\Output\Partial.avi';
tifFileName='.\Output\Whole.tif';
xH5FileName='.\Output\x.h5';
yH5FileName='.\Output\y.h5';
refH5FileName='.\Output\ref.h5';
variablesFileName='.\Output\variables.mat';
waveletFilterType='sym3';% 'coif2' 'sym3'
xIX=25; % used to be 28; 26
xIY=108; % used to be 112; 107
yIX=64; % assume  comes from TIR used to be 64
yIY=364; % assume  comes from TIR used to be 364 355
xRange=round(yIX/xIX*fifIX); %293;  % fixed 
yRange=round(yIY/xIY*fifIY); %416; % fixed
start1=8; % 1
start2=56; % 58
xDomain = fifIX;
yDomain = fifIY;

r1=yIY/xIY;
r2=yIX/xIX;

tile_x1=xIY;
tile_x2=xIX;

X1=fifIX;
X2=fifIY;


tile_xi1=yIY;
tile_xi2=yIX;

Xi1=round(X1*r1);
Xi2=round(X2*r2);

overlap_xi1=5;
overlap_xi2=5;

overlap_x1=ceil(overlap_xi1/r1);
overlap_x2=ceil(overlap_xi1/r2);


n_x1=ceil(X1/(tile_x1+overlap_x1-1));
n_x2=ceil(X2/(tile_x2+overlap_x2-1));

whiteInt=intmax('uint16')-10000; % RK was here


[NUM,TXT,tirData]=xlsread(inputExcel,'TIR');
[NUM,TXT,fifData]=xlsread(inputExcel,'FIF');
writerObj = VideoWriter(videoFileName,'Uncompressed AVI');
writerObjPartial = VideoWriter(videoFileNamePartial,'Uncompressed AVI');
open(writerObj);
open(writerObjPartial);

numData=0;
numXXX=0;
% Counting
for nRow=1:numRows
   numTIR= cell2mat(tirData(2+nRow,23));
   numData=numData+numTIR;
   numFIF= cell2mat(fifData(2+nRow,23));
   numXXX=numXXX+numFIF;
end

flag=uint16(ones(numData,1));
numData=0; 

for nRow=1:numRows
    endTimeFIF=cell2mat(fifData(2+nRow,21));
    startTimeFIF=cell2mat(fifData(2+nRow,20));
    numTIR= cell2mat(tirData(2+nRow,23));
    startTimeTIR=cell2mat(tirData(2+nRow,20));
    for nTIR=1:numTIR
        numData=numData+1;
        time_tir=startTimeTIR+(nTIR-1)/fpsTIR;
        if time_tir>=endTimeFIF
            flag(numData)=0;
        end
        if time_tir<=startTimeFIF
            flag(numData)=0;
        end
    end
end

numData=sum(flag);

tir=uint16(zeros(numData,tirIX,tirIY));
time_tir=zeros(numData,1);
id_tir=zeros(numData,1);
n_tir=zeros(numData,1);
n1_fif=zeros(numData,1);
n2_fif=zeros(numData,1);
fif_interpolated=zeros(numData,fifIX,fifIY);
whole_data=uint16(zeros(numData,tirIX+fifIX,tirIY+fifIY));whole_data(:,:)=whiteInt;
partial_data=uint16(zeros(numData,tirIX+fifIX,tirIY));partial_data(:,:)=whiteInt;
x_data=uint16(zeros(numData,xIX,xIY));
y_data=uint16(zeros(numData,yIX,yIY));
ref_data=uint16(zeros(numData,yIX,yIY));


numData=0; 
numNumData=0;
point_data1_index=[80 80];
point_data1=zeros(numData,4);


for nRow=1:numRows
   nRow
   numFIF= cell2mat(fifData(2+nRow,23));
   fif=uint16(zeros(numFIF,fifIX,fifIY)); 
   time_fif=zeros(numFIF,1);
   path=char(fifData(2+nRow,6));
   startTimeFIF=cell2mat(fifData(2+nRow,20));
   the_format='%03.0f';
   if numFIF<100
       the_format='%02.0f';
   end
   for nFIF=1:numFIF
       fileName=[path,'i1_',num2str(nFIF,the_format),'.tif'];
       [image,mapFIF]=imread(fileName);
       fif(nFIF,:,:)=image(:,:); % imshow(reshape(tir(end,:,:),size(image)))
       time_fif(nFIF)=startTimeFIF+(nFIF-1)/fpsFIF;
   end 
   numTIR= cell2mat(tirData(2+nRow,23));
   path=char(tirData(2+nRow,6));
   startTimeTIR=cell2mat(tirData(2+nRow,20));
   start1=round(cell2mat(fifData(2+nRow,25)));
   start2=round(cell2mat(fifData(2+nRow,26)));
   the_format='%03.0f';
   if numTIR>=1000
       the_format='%04.0f';
   end
   numNumData=numNumData+1;
   fileName=[path,'1_',num2str(1,the_format),'.tif'];
   [image,mapTIR]=imread(fileName);
   image_refernce=image;
   for nTIR=1:numTIR
       if flag(numNumData)
           numData=numData+1;
           fileName=[path,'1_',num2str(nTIR,the_format),'.tif'];
           [image,mapTIR]=imread(fileName);
           % norm_image_tir=double(image(:,:))./ double(whiteInt); % not normalzing
           norm_image_tir=double(image(:,:))./double(image_refernce(:,:)); % normalzing
           IMDEN = norm_image_tir; % wdenoise2(norm_image_tir,3,'Wavelet',waveletFilterType,'NoiseDirection','d','DenoisingMethod','UniversalThreshold','ThresholdRule','Soft');
           filtered_image_tir=IMDEN;%; norm_image_tir;%imgaussfilt(norm_image_tir,2);%*whiteInt;norm_image_tir;
           image=uint16(filtered_image_tir*double(whiteInt)); % RK was here jus remove 100
           tir(numData,:,:)=filtered_image_tir(:,:); % imshow(reshape(tir(end,:,:),size(image)))
           time_tir(numData)=startTimeTIR+(nTIR-1)/fpsTIR;
           [image_interpolated,n1,n2]=imageInterpolate(time_tir(numData),time_fif,fif,numFIF,fifIX,fifIY);
           fif_interpolated(numData,:,:)=image_interpolated;
           %whole_data(numData,1:tirIX,1:tirIY)=uint16(double(image(:,:))./double(image_refernce(:,:))*double(whiteInt));  % write normalaized in whole % image(:,:)
           whole_data(numData,1:tirIX,1:tirIY)=image(:,:);  % write normalaized in whole % image(:,:) normalized
           whole_data(numData,tirIX+1:tirIX+fifIX,tirIY+1:tirIY+fifIY)=image_interpolated(end:-1:1,1:end)'; % Transpose 90-deg Change % image_interpolated(:,:)
           whole_image=reshape(whole_data(numData,:,:),tirIX+fifIX,tirIY+fifIY); %imshow(whole_image);
           whole_image=im2double(whole_image);
           writeVideo(writerObj,whole_image);
           id_tir(numData)=nRow;
           n_tir(numData)=nTIR;
           n1_fif(numData)=n1;
           n2_fif(numData)=n2;
           the_format1='%04.0f';
           fileName=['.\Output\x\x',num2str(numData,the_format1),'.tif'];
           interpolated_image_fif=image_interpolated(end:-1:1,1:end)';
           start1_x=round((start1-1)/r1)+1;
           start2_x=round((start2-1)/r2)+1;
           end1_x=start1_x+tile_x1-1;
           end2_x=start2_x+tile_x2-1;
           cropped_image_fif=interpolated_image_fif(start2_x:end2_x,start1_x:end1_x);
           imwrite(cropped_image_fif,fileName);
           cropped_image_tir=image(:,(end-yIY+1):end);
           fileName=['.\Output\y\y',num2str(numData,the_format1),'.tif'];
           imwrite(cropped_image_tir,fileName);
           partial_data(numData,1:size(cropped_image_tir,1),1:size(cropped_image_tir,2))=cropped_image_tir;
           resized_cropped_image_fif=imresize(cropped_image_fif,[size(cropped_image_tir,1),size(cropped_image_tir,2)]);
           partial_data(numData,tirIX+1:(tirIX+size(resized_cropped_image_fif,1)),1:size(resized_cropped_image_fif,2))=resized_cropped_image_fif;
           partial_image=reshape(partial_data(numData,:,:),tirIX+fifIX,tirIY);
           partial_image=im2double(partial_image);
           writeVideo(writerObjPartial,partial_image);
           x_data(numData,:,:)=cropped_image_fif(:,:);
           y_data(numData,:,:)=cropped_image_tir(:,:);
           ref_data(numData,:,:)=image_refernce(:,(end-yIY+1):end);
           point_data1(numData,1)=time_tir(numData)-time_tir(1);
           point_data1(numData,2)=double(image_interpolated(point_data1_index(1),point_data1_index(2)))/63353;
           point_data1(numData,3)=time_fif(n1)-time_tir(1);
           point_data1(numData,4)=double(fif(n1,point_data1_index(1),point_data1_index(2)))/63353;
       end
   end
end
%%plot(point_data1(:,1),point_data1(:,2),'b-',point_data1(:,3),point_data1(:,4),'ro')
offset_time=time_tir(1);
point_data1_index=[11 11];
% plot(time_tir-offset_time,interp1(time_fif,double(fif(:,point_data1_index(1),point_data1_index(2)))/63353.0,time_tir,'cubic') ,'bo',time_fif-offset_time,double(fif(:,point_data1_index(1),point_data1_index(2)))/63353.0,'r-')
% hold on
% grid on
% legend('Interpolated (Cubic)','Original Data')
% savefig(['./Output/Time3.fig']);RK_save2pdf(['./Output/Time3.pdf']);
% hold off

if exist(xH5FileName, 'file')==2
  delete(xH5FileName);
  delete(yH5FileName);
  delete(refH5FileName);
end
h5create(xH5FileName, '/x', size(x_data),'Datatype','uint16');
h5create(yH5FileName, '/y', size(y_data),'Datatype','uint16');
h5create(refH5FileName, '/ref', size(ref_data),'Datatype','uint16');
h5write(xH5FileName, '/x', x_data)
h5write(yH5FileName, '/y', y_data)
h5write(refH5FileName, '/ref', ref_data)

close(writerObj);
close(writerObjPartial);


% Whole Data xx



numXX=0;
for nRow=1:numRows
   numFIF= cell2mat(fifData(2+nRow,23));
   for nFIF=1:numFIF
       for n1=1:n_x1
       for n2=1:n_x2
            numXX=numXX+1;
        end
        end
   end  
end
xx_data=uint16(zeros(numXX,xIX,xIY));
numXX=0;
for nRow=1:numRows
   nRow
   numFIF= cell2mat(fifData(2+nRow,23));
   fif=uint16(zeros(numFIF,fifIX,fifIY));
   path=char(fifData(2+nRow,6));
   the_format='%03.0f';
   if numFIF<100
       the_format='%02.0f';
   end
   for nFIF=1:numFIF
       fileName=[path,'i1_',num2str(nFIF,the_format),'.tif'];
       [image,mapFIF]=imread(fileName);
       the_format1='%06.0f';
       image_fif=image(end:-1:1,1:end)';
       %cropped_image_fif=interpolated_image_fif(21:48,1:112);
       %imwrite(cropped_image_fif,fileName);
       for n1=1:n_x1
       for n2=1:n_x2
%            the_colororder=the_colororder+1;
            start1=(n1-1)*(tile_x1-overlap_x1)+1;
            start2=(n2-1)*(tile_x2-overlap_x2)+1;
            if n1==n_x1
                start1=X1-tile_x1-1+1;
            end
            if n2==n_x2
                start2=X2-tile_x2-1+1;
            end
            numXX=numXX+1;
            cropped_image_fif=image_fif(start2:start2+tile_x2-1, start1:start1+tile_x1-1);
            fileName=['.\Output\xx\xx',num2str(numXX,the_format1),'.tif'];
            imwrite(cropped_image_fif,fileName);
            xx_data(numXX,:,:)=cropped_image_fif(:,:);
        end
        end
   end  
end
xxH5FileName='.\Output\xx.h5';
if exist(xxH5FileName, 'file')==2
  delete(xxH5FileName);
end
h5create(xxH5FileName, '/xx', size(xx_data),'Datatype','uint16');
h5write(xxH5FileName, '/xx', xx_data)

save(variablesFileName,'id_tir','n_tir','n1_fif','n2_fif')

